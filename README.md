# Local MongoDB Development Database

A docker image that is seeded with inital data for a convenient and consistant development experience.

## How to use

From the command line navigate to the project's root and use the following command:

```
docker-compose up -d
```

The database listens for connections on `mongodb://localhost:27017/development-db`.
